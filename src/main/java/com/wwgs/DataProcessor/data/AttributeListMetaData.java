package com.wwgs.DataProcessor.data;

public class AttributeListMetaData 
{
	public AttributeListMetaData() {
	}
	
	private String code;
	private String label;
	private String attribute;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	
}
