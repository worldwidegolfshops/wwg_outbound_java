package com.wwgs.DataProcessor.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AttributeMetaData {

	public AttributeMetaData() {
	}
	private String type;
	private String code;
	private String name;
	private Map<String,List<AttributeListMetaData>> attrOptionsMaps;
	
	public Map<String, List<AttributeListMetaData>> getAttrOptionsMaps() {
		return attrOptionsMaps;
	}
	public void setAttrOptionsMaps(Map<String, List<AttributeListMetaData>> attrOptionsMaps) {
		this.attrOptionsMaps = attrOptionsMaps;
	}
	private Map<String,String> attrCdName;
	

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String, String> getAttrCdName() {
		return attrCdName;
	}
	public void setAttrCdName(Map<String, String> attrCdName) {
		this.attrCdName = attrCdName;
	}
	
}
