package com.wwgs.DataProcessor.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Product {
		
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(String productId) {
		this.productId = productId;
	}
	private String productId;
	private String parentModelId;
	private String categoriesID;
	private String categoriesLabel;
	private boolean enabled;
	private String productFamily;
	private String productType;
	private HashMap<String,HashMap<String,Object>> attributeValue;
	private List<String> productHeaders;
	public HashMap<String, HashMap<String, Object>> getAttributeValue() {
		return attributeValue;
	}
	public void setAttributeValue(HashMap<String, HashMap<String, Object>> attributeValue) {
		this.attributeValue = attributeValue;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}

	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public List<String> getProductHeaders() {
		return productHeaders;
	}
	public void setProductHeaders(List<String> productHeaders) {
		this.productHeaders = productHeaders;
	}
	
	public String getParentModelId() {
		return parentModelId;
	}
	public void setParentModelId(String parentModelId) {
		this.parentModelId = parentModelId;
	}
	public String getProductFamily() {
		return productFamily;
	}
	public void setProductFamily(String productFamily) {
		this.productFamily = productFamily;
	}
	public String getCategoriesID() {
		return categoriesID;
	}
	public void setCategoriesID(String categoriesID) {
		this.categoriesID = categoriesID;
	}
	public String getCategoriesLabel() {
		return categoriesLabel;
	}
	public void setCategoriesLabel(String categoriesLabel) {
		this.categoriesLabel = categoriesLabel;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	
	
	}
