package com.wwgs.DataProcessor.configuration;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("config")
public class GlobalConfiguration {

	private String user;
	private String password;
	private String apptoken;
	private String authurl;
	private String producturldelta;
	private String producturlall;
	private String productmodelurlall;
	public String getProducturlall() {
		return producturlall;
	}
	public void setProducturlall(String producturlall) {
		this.producturlall = producturlall;
	}
	private String attributedataurl;
	private String attributeoptionsurl;
	private String outboundfolder;
	private String resourcefolder;
	private String searchParamBudgetFull;
	private String searchParamWWGFull;
	private String searchParamCAFull;
	private String searchParamBudgetDelta;
	private String searchParamWWGDelta;
	private String searchParamCADelta;
	private String categorieslabelurl;
	private String RPROreportpath;
	private String RPROreportexportpath;

	private String searchParamModelBudgetFull;
	private String searchParamModelWWGFull;
	private String searchParamModelCAFull;
	private String searchParamModelBudgetDelta;
	private String searchParamModelWWGDelta;
	private String searchParamModelCADelta;
	
	public String getSearchParamModelBudgetFull() {
		return searchParamModelBudgetFull;
	}
	public void setSearchParamModelBudgetFull(String searchParamModelBudgetFull) {
		this.searchParamModelBudgetFull = searchParamModelBudgetFull;
	}
	public String getSearchParamModelWWGFull() {
		return searchParamModelWWGFull;
	}
	public void setSearchParamModelWWGFull(String searchParamModelWWGFull) {
		this.searchParamModelWWGFull = searchParamModelWWGFull;
	}
	public String getSearchParamModelCAFull() {
		return searchParamModelCAFull;
	}
	public void setSearchParamModelCAFull(String searchParamModelCAFull) {
		this.searchParamModelCAFull = searchParamModelCAFull;
	}
	public String getSearchParamModelBudgetDelta() {
		return searchParamModelBudgetDelta;
	}
	public void setSearchParamModelBudgetDelta(String searchParamModelBudgetDelta) {
		this.searchParamModelBudgetDelta = searchParamModelBudgetDelta;
	}
	public String getSearchParamModelWWGDelta() {
		return searchParamModelWWGDelta;
	}
	public void setSearchParamModelWWGDelta(String searchParamModelWWGDelta) {
		this.searchParamModelWWGDelta = searchParamModelWWGDelta;
	}
	public String getSearchParamModelCADelta() {
		return searchParamModelCADelta;
	}
	public void setSearchParamModelCADelta(String searchParamModelCADelta) {
		this.searchParamModelCADelta = searchParamModelCADelta;
	}
	public String getRPROreportpath() {
		return RPROreportpath;
	}
	public void setRPROreportpath(String RPROreportpath) {
		RPROreportpath = RPROreportpath;
	}
	public String getRPROreportexportpath() {
		return RPROreportexportpath;
	}
	public void setRPROreportexportpath(String RPROreportexportpath) {
		RPROreportexportpath = RPROreportexportpath;
	}
	public String getSearchParamBudgetFull() {
		return searchParamBudgetFull;
	}
	public void setSearchParamBudgetFull(String searchParamBudgetFull) {
		this.searchParamBudgetFull = searchParamBudgetFull;
	}
	public String getSearchParamWWGFull() {
		return searchParamWWGFull;
	}
	public void setSearchParamWWGFull(String searchParamWWGFull) {
		this.searchParamWWGFull = searchParamWWGFull;
	}
	public String getSearchParamCAFull() {
		return searchParamCAFull;
	}
	public void setSearchParamCAFull(String searchParamCAFull) {
		this.searchParamCAFull = searchParamCAFull;
	}
	public String getSearchParamBudgetDelta() {
		return searchParamBudgetDelta;
	}
	public void setSearchParamBudgetDelta(String searchParamBudgetDelta) {
		this.searchParamBudgetDelta = searchParamBudgetDelta;
	}
	public String getSearchParamWWGDelta() {
		return searchParamWWGDelta;
	}
	public void setSearchParamWWGDelta(String searchParamWWGDelta) {
		this.searchParamWWGDelta = searchParamWWGDelta;
	}
	public String getSearchParamCADelta() {
		return searchParamCADelta;
	}
	public void setSearchParamCADelta(String searchParamCADelta) {
		this.searchParamCADelta = searchParamCADelta;
	}
	public String getResourcefolder() {
		return resourcefolder;
	}
	public void setResourcefolder(String resourcefolder) {
		this.resourcefolder = resourcefolder;
	}
	public String getProducturldelta() 
	{
		String encodedSearchParam = "{\"updated\":[{\"operator\":\"SINCE LAST N DAYS\",\"value\":20}]}";
		try {
			encodedSearchParam = URLEncoder.encode(encodedSearchParam, "UTF-8");
			encodedSearchParam = encodedSearchParam + "&pagination_type=search_after&limit=100";
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		producturldelta = producturldelta + encodedSearchParam;
		return producturldelta;
	}
	public void setProducturldelta(String producturldelta) {
		this.producturldelta = producturldelta;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getApptoken() {
		return apptoken;
	}
	public void setApptoken(String apptoken) {
		this.apptoken = apptoken;
	}
	public String getAuthurl() {
		return authurl;
	}
	public void setAuthurl(String authurl) {
		this.authurl = authurl;
	}
	public String getAttributedataurl() {
		return attributedataurl;
	}
	public void setAttributedataurl(String attributedataurl) {
		this.attributedataurl = attributedataurl;
	}
	public String getAttributeoptionsurl() {
		return attributeoptionsurl;
	}
	public void setAttributeoptionsurl(String attributeoptionsurl) {
		this.attributeoptionsurl = attributeoptionsurl;
	}
	public String getOutboundfolder() {
		return outboundfolder;
	}
	public void setOutboundfolder(String outboundfolder) {
		this.outboundfolder = outboundfolder;
	}
	public String getCategorieslabelurl() {
		return categorieslabelurl;
	}
	public void setCategorieslabelurl(String categorieslabelurl) {
		this.categorieslabelurl = categorieslabelurl;
		
	}
	public String getProductmodelurlall() {
		return productmodelurlall;
	}
	public void setProductmodelurlall(String productmodelurlall) {
		this.productmodelurlall = productmodelurlall;
	}
	
	
	
	}
