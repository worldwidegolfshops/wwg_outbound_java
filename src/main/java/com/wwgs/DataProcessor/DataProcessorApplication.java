package com.wwgs.DataProcessor;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.wwgs.DataProcessor.api.ProductController;
import com.wwgs.DataProcessor.configuration.GlobalConfiguration;
import com.wwgs.DataProcessor.service.ProductDataAccessService;

@SpringBootApplication
@ConfigurationProperties
//@EnableScheduling
public class DataProcessorApplication implements CommandLineRunner{
	@Autowired
	private GlobalConfiguration config;
	
	
	public static void main(String[] args) {
		SpringApplication.run(DataProcessorApplication.class, args);
	
	}
	
	@Override
	public void run(String... args) throws Exception 
	{
	/*	ProductDataAccessService productService = new ProductDataAccessService(config);
		String targets = null;
		String days = null;
		if(args.length > 1)
		{
			if(args[0] != null)
				targets = args[0];
			if(args[0] != null)
				days = args[1];
			productService.getProductData(targets,days);
		}
		else
			productService.getProductData(targets,days);
			*/
	}
	
}