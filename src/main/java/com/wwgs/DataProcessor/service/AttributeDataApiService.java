package com.wwgs.DataProcessor.service;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wwgs.DataProcessor.configuration.GlobalConfiguration;
import com.wwgs.DataProcessor.data.AttributeListMetaData;
import com.wwgs.DataProcessor.data.AttributeMetaData;

public class AttributeDataApiService {
	public AttributeDataApiService(GlobalConfiguration config) {
		this.config = config;
	}
	
	private GlobalConfiguration config;
	ApiProcessorService api = new ApiProcessorService(config);
	static List<AttributeMetaData> attrDataAll = new ArrayList<AttributeMetaData>();
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    
    public void getAttributeData(String url) {
		System.out.println("Fetching attribute Data...Start"+dateFormat.format(System.currentTimeMillis()));
		String attributeDataOutput = null;
		String attributeUrl = null;
		attributeUrl=url;
		if(attributeUrl=="")
		{
			System.out.println("Attr url:"+config.getAttributedataurl());
			attributeDataOutput = api.requestData(config.getAttributedataurl());
			
		}
		else
			attributeDataOutput = api.requestData(url);
		JSONObject attrDataResponseObj = new JSONObject(attributeDataOutput);
		processAttributeData(attributeDataOutput);
		if(attrDataResponseObj.getJSONObject("_links").has("next"))
		{
			attributeUrl = attrDataResponseObj.getJSONObject("_links")
					.getJSONObject("next").getString("href");
			getAttributeData(attributeUrl);
		}		
		System.out.println("Fetching attribute Data...End"+dateFormat.format(System.currentTimeMillis()));
	}
	
	public void processAttributeData(String attribueResponse) {
		System.out.println("Processing attribute Data...Start"+dateFormat.format(System.currentTimeMillis()));

		JSONObject attrDataResponseObj = new JSONObject(attribueResponse);
		JSONArray attributeDataArray = attrDataResponseObj.getJSONObject("_embedded").getJSONArray("items");
		
		HashMap<String, String> attrCdName = new HashMap<String, String>();
		for (int i = 0; i < attributeDataArray.length(); i++) 
		{
			HashMap<String, List<AttributeListMetaData>> attrOptionsMaps = new HashMap<String, List<AttributeListMetaData>>();
			AttributeMetaData attributeMetaData = new AttributeMetaData();
			String attribute_code = attributeDataArray.getJSONObject(i).getString("code");
			//validate for number (integer) type, including negative values
			String attribute_type = attributeDataArray.getJSONObject(i).getString("type");
			String attribute_name = null;
			JSONObject attribute_name_obj = attributeDataArray.getJSONObject(i).getJSONObject("labels");
			if (attribute_name_obj.length() != 0) {
				attribute_name = attribute_name_obj.get("en_US").toString();
			}
			attributeMetaData.setCode(attribute_code);
			attributeMetaData.setType(attribute_type);
			attributeMetaData.setName(attribute_name);
			attrCdName.put(attribute_code, attribute_name);
			attributeMetaData.setAttrCdName(attrCdName);
			//			System.out.println("Attribute Name from the response: "+attributeMetaData[i].getName());
			if (attribute_type.equals("pim_catalog_simpleselect")) 
			{
				AttributeLabelApiService attrLabelApi = new AttributeLabelApiService(config);
				attrLabelApi.getAttributeLabels(attribute_code, "");
				attrOptionsMaps.put(attribute_code, attrLabelApi.attributeListMetaData);
				
			}
			if(attribute_type.equals("pim_catalog_metric"))
			{
				
			}
			attributeMetaData.setAttrOptionsMaps(attrOptionsMaps);
		//	attributeMetaData.setAttrOptionsMaps(attrOptionsMaps);
			
			attrDataAll.add(attributeMetaData);
		}
		System.out.println("Processing attribute Data...End"+dateFormat.format(System.currentTimeMillis()));

	}
	


}
