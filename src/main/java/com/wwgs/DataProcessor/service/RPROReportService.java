package com.wwgs.DataProcessor.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.RFC4180Parser;
import com.opencsv.RFC4180ParserBuilder;
import com.wwgs.DataProcessor.configuration.GlobalConfiguration;
import com.wwgs.DataProcessor.data.AttributeListMetaData;
import com.wwgs.DataProcessor.data.AttributeMetaData;
import com.wwgs.DataProcessor.data.Product;

public class RPROReportService {
	private final GlobalConfiguration config;
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

	public RPROReportService(GlobalConfiguration config) {
		this.config = config;
	}

	public void writeReport(HashMap reportData, String fileName) throws IOException {
		String inboundFolder = config.getRPROreportpath();
		String outboundFolder = config.getRPROreportexportpath();

		String delimiter = "|";


		List<String> headers = new ArrayList<String>();
		List<String> finalHeaders = new ArrayList<String>();
		finalHeaders.add("");
		int remove = 0;
		try {
			System.out.println("Reading file..");
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			CSVWriter writer = new CSVWriter(new FileWriter(
					outboundFolder + "RPROITEMS_REPORT.CSV"),
					delimiter.charAt(0), '"', '"', "\n");
			String csvFile = inboundFolder;
			if (new File(csvFile).exists()) {
				//updated seperator for newline to allow commas in file
				RFC4180Parser rfc4180Parser = new RFC4180ParserBuilder().withSeparator('\n').build();

				CSVReader reader = new CSVReaderBuilder(new FileReader(csvFile)).withCSVParser(rfc4180Parser).build();

				List<String[]> rproData = reader.readAll();
				System.out.println("Data read..");
				StringTokenizer tokens = new StringTokenizer(rproData.get(0)[0], delimiter);
				int i = 0;
				while (tokens.hasMoreTokens()) {
					String header = tokens.nextToken();
					if (header.equals("ALU"))
						header = "sku";
					headers.add(i, header);
					i++;
				}
				//w
				System.out.println("Total Products : " + rproData.size());
				for (int j = 1; j < rproData.size(); j++) 
				{
					try {
						int headerIndex = 0;
						List<String> productDataAll = new ArrayList<String>();
						String productAttrVal = null;
						String productAttrPrevVal = null;
						//System.out.println(rproData.get(j)[0]);
						StringTokenizer productData = new StringTokenizer(rproData.get(j)[0], delimiter, true);

						boolean removeProduct = false;

						int upcIndex = headers.indexOf("LOCAL_UPC");
						int flagIndex = headers.indexOf("FLAG");
						int dcsIndex = headers.indexOf("DCS_CODE");
						//System.out.println("DCS_CODE " + dcsIndex);

						while (productData.hasMoreTokens()) {
							String header = null;

							productAttrVal = productData.nextToken();

							//System.out.println(productAttrVal + "");

							if (headerIndex == upcIndex && (productAttrVal.equals("|") && productAttrPrevVal.equals("|"))) {
								removeProduct = true;
								break;
							}
							//filter for request to remove products with UPC value of 5
							if (headerIndex == upcIndex && (productAttrVal.equals("5"))) {
								removeProduct = true;
								break;
							}

							String [] allCheckVal = {"O" , "X" , "N" , "Z" , "U"};

							for (String strTmp : allCheckVal) {
								if (headerIndex == dcsIndex && (productAttrVal.startsWith(strTmp))) {
									removeProduct = true;
									break;
								}
							}

							if (productAttrVal.equals(delimiter) && productAttrVal.equals(productAttrPrevVal)) {
								header = headers.get(headerIndex);
								productAttrVal = "";
								productDataAll.add(productAttrVal);
								productAttrPrevVal = delimiter;
								headerIndex++;
							} else {
								if (!(productAttrVal.equals(delimiter))) {
									header = headers.get(headerIndex);
									if (headerIndex == upcIndex) {
										String updatedUPC = transformUPC(productAttrVal);
										productAttrVal = updatedUPC;
									}
									if (headerIndex == flagIndex) 
									{
										String updatedFlag = "";
										if(productAttrVal.equals("0")) updatedFlag = "0"; 
										else if (productAttrVal.equals("2")) updatedFlag = "1";
										else updatedFlag = productAttrVal;									
										productAttrVal = updatedFlag;
									}
									productDataAll.add(productAttrVal);
									productAttrPrevVal = productAttrVal;
									headerIndex++;
								} else
									productAttrPrevVal = delimiter;
							}
						}

						if (removeProduct) {
							remove++;
							//System.out.println("Product Removed: "+productDataAll.toString());

						}
						if (!removeProduct) {
							//					System.out.println(productDataAll.toString());
							writer.writeNext(productDataAll.toArray((new String[tokens.countTokens()])));
						}

					}
					catch(Exception e)
					{
						e.printStackTrace();
						continue;
					}
				}
				System.out.println("No. of products deleted: " + remove);
				System.out.println("Write complete...");
				writer.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	public static String transformUPC(String upc) {
		String updatedUPC = upc;
		if (updatedUPC.length() >= 12)
			updatedUPC = upc;
		else {
			int paddingSize = 12;
			updatedUPC = StringUtils.leftPad(updatedUPC, paddingSize, "0");
		}
		return updatedUPC;
	}	

}
