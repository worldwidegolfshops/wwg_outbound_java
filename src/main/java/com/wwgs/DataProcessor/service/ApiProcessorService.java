package com.wwgs.DataProcessor.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wwgs.DataProcessor.configuration.GlobalConfiguration;
@Component
public class ApiProcessorService {
	@Autowired
	public ApiProcessorService(GlobalConfiguration config) {
		this.config = config;
	}
	private GlobalConfiguration config;
	static String sessionAuthToken;
	String authUrl = null;
	String user = null;
	
	public void setSessionAuthToken() 
	{
		String alloutput = "";
		HttpClient httpClient = HttpClientBuilder.create().build();
		// System.out.println("URL to be invoked" + urlString);

		HttpResponse responseData = null;
		try {
			HttpPost postRequest = new HttpPost(config.getAuthurl());
		//	String json = "{\"grant_type\": \"password\",\"username\": \"Reshmi\",\"password\": \"Wwgs@1234\"}";
			String json = "{\"grant_type\": \"password\",\"username\": \""+config.getUser()+"\",\"password\": \""+config.getPassword()+"\"}";
			System.out.println(json);
			StringEntity entity;
			entity = new StringEntity(json);
			postRequest.setEntity(entity);
			postRequest.setHeader("Authorization", config.getApptoken());
			postRequest.setHeader("Content-Type", "application/json");
			postRequest.setHeader("Accept", "application/json");
			responseData = httpClient.execute(postRequest);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader((responseData.getEntity().getContent())));
			String output;
			while ((output = br.readLine()) != null) {
				alloutput = alloutput + output;
			}
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("OUTPUT: " + alloutput);
		JSONObject authResponse = new JSONObject(alloutput);
		sessionAuthToken = authResponse.getString("access_token");
	}

	public String requestData(String urlString) {
		String alloutput = "";
		//System.out.println("ALL URL STRING: " + urlString);
		try {
			String authToken = null;
			authToken = "Bearer " + sessionAuthToken;
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet getRequest = new HttpGet(urlString);
			getRequest.addHeader("Authorization", authToken);
			HttpResponse responseData = null;
			try {
				responseData = httpClient.execute(getRequest);
			//	System.out.println(responseData.toString());
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((responseData.getEntity().getContent())));

			String output;
			
			while ((output = br.readLine()) != null) {
				alloutput = alloutput + output;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return alloutput;
	}
}
