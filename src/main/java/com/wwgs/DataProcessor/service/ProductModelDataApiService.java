package com.wwgs.DataProcessor.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import com.wwgs.DataProcessor.configuration.GlobalConfiguration;
import com.wwgs.DataProcessor.data.Product;

public class ProductModelDataApiService {
	public ProductModelDataApiService(GlobalConfiguration config) {
		this.config = config;
	}
	
	private GlobalConfiguration config;
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	List<Product> productModelDataAll = new ArrayList<Product>();
	String previousUrl = "";
	
	

	public void getProductModelData(String url,String target,String days) 
	{
		System.out.println("Product Model data size.."+productModelDataAll.size());
		ApiProcessorService api = new ApiProcessorService(config);
		if(productModelDataAll.size() >= 1000)
		{
			api.setSessionAuthToken();
			System.out.println("NEW TOKEN");
			previousUrl = url;
			System.out.println("Greater than 10...");
			return;
		} 
		System.out.println("Getting Product Model Data...Start"+dateFormat.format(System.currentTimeMillis()));
		String productModelDataOutput = null;
		String productUrl = null;
		productUrl=url;
		String encodedSearchParamWWG = "";
		String encodedSearchParamBG = "";
		String encodedSearchParamCA = "";
		
		try 
		{
			encodedSearchParamWWG = URLEncoder.encode(config.getSearchParamModelWWGFull(), "UTF-8");
			encodedSearchParamBG = URLEncoder.encode(config.getSearchParamModelBudgetFull(), "UTF-8");
			encodedSearchParamCA = URLEncoder.encode(config.getSearchParamModelCAFull(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		String productUrlWWG = config.getProductmodelurlall() + encodedSearchParamWWG;
		String productUrlBG = config.getProductmodelurlall() + encodedSearchParamBG;
		String productUrlCA = config.getProductmodelurlall() + encodedSearchParamCA;
		
		if(productUrl.equals(null) || productUrl=="")
		{
				if(target.equals("WORLD_WIDE_GOLF")) {
					System.out.println("productURLWWG: " + productUrlWWG);
					productModelDataOutput = api.requestData(productUrlWWG);
				} else if(target.equals("BUDGET_GOLF")) {
					System.out.println("productURLBG: " + productUrlBG);
					productModelDataOutput = api.requestData(productUrlBG);
				} else if(target.equals("CHANNEL_ADVISOR")) {
					System.out.println("productURLCA: " + productUrlCA);					
					productModelDataOutput = api.requestData(productUrlCA);
				}
		}
		else
		{
			System.out.println("productURL: " + productUrl);
			productModelDataOutput = api.requestData(productUrl);
			
		}

		JSONObject attrDataResponseObj = new JSONObject(productModelDataOutput);
		processProductData(productModelDataOutput);
		if(attrDataResponseObj.getJSONObject("_links").has("next"))
		{
			productUrl = attrDataResponseObj.getJSONObject("_links")
					.getJSONObject("next").getString("href");
			getProductModelData(productUrl,"",null);
		}
		System.out.println("Getting Product Model Data...End"+dateFormat.format(System.currentTimeMillis()));
	}
	
	
	public void processProductData(String productModelDataOutput) {
		System.out.println("Processing Product Model Data...Start"+dateFormat.format(System.currentTimeMillis()));
		//System.out.println("OUTPUT DATA " + productModelDataOutput);
		HashMap allCategoryCache = new HashMap();
		JSONObject productDataResponseObj = new JSONObject(productModelDataOutput);
		// Get product data
		JSONArray productDataArray = productDataResponseObj.getJSONObject("_embedded").getJSONArray("items");
		for (int i = 0; i < productDataArray.length(); i++) {
			Product productData = new Product();
			String productId = (String) productDataArray.getJSONObject(i).get("code");
			//boolean productEnabled = productDataArray.getJSONObject(i).getBoolean("enabled");
			productData.setProductId(productId);
		//	System.out.println(productDataArray.getJSONObject(i).get("parent"));
			if(productDataArray.getJSONObject(i).get("parent").toString() != "null")
				productData.setParentModelId((String) productDataArray.getJSONObject(i).get("parent"));
			if(productDataArray.getJSONObject(i).get("family").toString() != "null")
				productData.setProductFamily((String) productDataArray.getJSONObject(i).get("family"));
			productData.setEnabled(true);
			productData.setProductType("PRODUCT");
			//add code for categories code retrieval, then get categories names
			
			if(!productDataArray.getJSONObject(i).getJSONArray("categories").isEmpty()) {
				String allCategoriesID = "";
				String allCategories = "";
				JSONArray allCategoryIDs = productDataArray.getJSONObject(i).getJSONArray("categories");
				for(int t=0; t < allCategoryIDs.length(); t++) {
					//System.out.println("CAT ID: " + allCategoryIDs.getString(t));
					try {
						allCategoriesID += allCategoryIDs.getString(t) + ", ";
						if(allCategoryCache.containsKey(allCategoryIDs.getString(t))) {
							allCategories += allCategoryCache.get(allCategoryIDs.getString(t)) + ",";
							//System.out.println("GOT CATEGORY FROM HASH: " + allCategoryIDs.getString(t));
						} else {
							ApiProcessorService api2 = new ApiProcessorService(config);
							//System.out.println("Getting Category " + allCategoryIDs.getString(t) + " "+dateFormat.format(System.currentTimeMillis()));
							String categoryUrl = config.getCategorieslabelurl();
							//System.out.println("CATEGORY URL: " + categoryUrl);
							
							String categoryApiOutput = api2.requestData(categoryUrl + URLEncoder.encode(allCategoryIDs.getString(t), "UTF-8"));
							//System.out.println("CATEGORY API OUTPUT: " + categoryApiOutput);
							JSONObject categoryResponseObj = new JSONObject(categoryApiOutput);
							JSONObject categoryLabelsObj = categoryResponseObj.getJSONObject("labels");
							if (categoryLabelsObj.length() != 0) {
								allCategories += categoryLabelsObj.get("en_US").toString() + ", ";
								allCategoryCache.put(allCategoryIDs.getString(t),categoryLabelsObj.get("en_US").toString());
							}
						}
					} catch (Exception e) {
						System.out.println("ERROR IN CAT RET: " + e);
					}
				}
				//System.out.println("CATEGORY IDS: " + allCategoriesID);
				//System.out.println("CATEGORY LABELS: " + allCategories);
				productData.setCategoriesID(allCategoriesID.substring(0, allCategoriesID.lastIndexOf(",")));
				productData.setCategoriesLabel(allCategories.substring(0, allCategories.lastIndexOf(",")));
			}
			
			
			JSONObject productAttributesList = productDataArray.getJSONObject(i).getJSONObject("values");
			HashMap<String, HashMap<String,Object>> productAttributes = new HashMap<String, HashMap<String,Object>>();
			
			for (String productAttrName : productAttributesList.keySet()) 
			{
				JSONArray attributeValue = (JSONArray) productAttributesList.get(productAttrName);
				boolean channelSpecificVal = false;
				if((attributeValue.length())>1)
					channelSpecificVal=true;
				HashMap<String, Object> productAttributesChannel = new HashMap<String, Object>();
				for (int j = 0; j < attributeValue.length(); j++) 
				{
					String channel = "common";
					String data = null;
					if(channelSpecificVal)
					{
						channel=String.valueOf(attributeValue.getJSONObject(j).get("scope"));
						if (attributeValue.getJSONObject(j).get("data") instanceof Boolean) {
							Boolean booleanValue = (Boolean) attributeValue.getJSONObject(j).get("data");
							productAttributesChannel.put(channel, booleanValue);
						} 
						
						
						else if(attributeValue.getJSONObject(j).get("data") instanceof JSONArray)
						{
						//	System.out.println("This might be a metric value??");
						}
						else
						{
						data = String.valueOf(attributeValue.getJSONObject(j).get("data"));
						productAttributesChannel.put(channel, data);
						}
					}
					else
					{
						if (attributeValue.getJSONObject(j).get("data") instanceof Boolean) {
							Boolean booleanValue = (Boolean) attributeValue.getJSONObject(j).get("data");
							productAttributesChannel.put(channel, booleanValue);
						} 
						else if(attributeValue.getJSONObject(j).get("data") instanceof Object)
						{
							Object dataTest = (Object) attributeValue.getJSONObject(j).get("data").getClass();
							//System.out.println(dataTest);
							if(attributeValue.getJSONObject(j).get("data") instanceof JSONObject)
							{
								JSONObject dataMetric = (JSONObject) attributeValue.getJSONObject(j).get("data");
								String valueMetric = (String) dataMetric.get("amount");
								String unitMetric = (String) dataMetric.get("unit");
								int dataLength = dataMetric.length();
							//	System.out.println("data length.."+dataLength+" :value: "+valueMetric+" :unitMetric: "+unitMetric);
							//	System.out.println("This might be a metric value??"+dataTest.toString());
								List<String> dataWithMetric = new ArrayList<String>();
								dataWithMetric.add(valueMetric);
								dataWithMetric.add(unitMetric);
								productAttributesChannel.put(channel, dataWithMetric);
							}
							else
							{
								data = String.valueOf(attributeValue.getJSONObject(j).get("data"));
								productAttributesChannel.put(channel, data);
							}
						}
						else
						{
						data = String.valueOf(attributeValue.getJSONObject(j).get("data"));
						productAttributesChannel.put(channel, data);
						}
					}
				}
				productAttributes.put(productAttrName, productAttributesChannel);
			}
			productData.setAttributeValue(productAttributes);
			productModelDataAll.add(productData);
			
		}
		System.out.println("Processing product Data...End"+dateFormat.format(System.currentTimeMillis()));
	
	}
}
