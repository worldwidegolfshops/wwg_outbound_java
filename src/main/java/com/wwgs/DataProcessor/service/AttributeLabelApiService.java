package com.wwgs.DataProcessor.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.wwgs.DataProcessor.configuration.GlobalConfiguration;
import com.wwgs.DataProcessor.data.AttributeListMetaData;
import com.wwgs.DataProcessor.data.Product;

public class AttributeLabelApiService {

	public AttributeLabelApiService(GlobalConfiguration config) {
		this.config = config;
	}

	private GlobalConfiguration config;
	List<AttributeListMetaData> attributeListMetaData = new ArrayList<AttributeListMetaData>(10000);
	ApiProcessorService api = new ApiProcessorService(config);

	public void getAttributeLabels(String attributeCode,String attributeOptionsUrl)
	{
		String attributeListOutput;
		if(attributeOptionsUrl=="")
			attributeListOutput = api.requestData(config.getAttributeoptionsurl()+attributeCode+"/options?limit=100");
		else
			attributeListOutput = api.requestData(attributeOptionsUrl);
		JSONObject attrListResponseObj = new JSONObject(attributeListOutput);
		processAttributeLables(attributeListOutput);
		if(attrListResponseObj.getJSONObject("_links").has("next"))
		{
			attributeOptionsUrl = attrListResponseObj.getJSONObject("_links")
					.getJSONObject("next").getString("href");
			getAttributeLabels("",attributeOptionsUrl);
		}
	}

	public void processAttributeLables(String attributeLabelOutput)
	{
		JSONObject attrLabelResponseObj = new JSONObject(attributeLabelOutput);

		JSONArray attributeOptionsArray = attrLabelResponseObj.getJSONObject("_embedded").getJSONArray("items");
		for (int j = 0; j < attributeOptionsArray.length(); j++)
		{
			AttributeListMetaData attrMetaData = new AttributeListMetaData();
			String attributeList_code = attributeOptionsArray.getJSONObject(j).getString("code");
			JSONObject attributeList_labels = attributeOptionsArray.getJSONObject(j).getJSONObject("labels");
			attrMetaData.setCode(attributeList_code);
			try {
				attrMetaData.setLabel(attributeList_labels.get("en_US").toString());
				attributeListMetaData.add(attrMetaData);
			} catch (org.json.JSONException je) {
				String attribute_name = attributeOptionsArray.getJSONObject(j).getString("attribute");
				System.out.println("No English Label found in attribute " + attribute_name + "for option " + attributeList_code);
				attrMetaData.setLabel("");
				attributeListMetaData.add(attrMetaData);
			}
		}

	}
}
