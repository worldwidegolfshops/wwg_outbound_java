package com.wwgs.DataProcessor.service;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.wwgs.DataProcessor.configuration.GlobalConfiguration;
import com.wwgs.DataProcessor.data.Product;
@Controller
public class ProductDataAccessService {

	private final GlobalConfiguration config;
	
	public ProductDataAccessService(GlobalConfiguration config) {
		this.config = config;
	}
	
	@Value("${spring.application.name}")
	private String appName;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	SimpleDateFormat fileDateFormat = new SimpleDateFormat("dd.MM.yyyy");
	
	HashMap finalReport = new HashMap();
	
	String url = "";
	boolean delta = true;
	
	public String getReport() {
		System.out.println("Inside RPRO Report Generator:"+appName);
		ApiProcessorService api = new ApiProcessorService(config);
		api.setSessionAuthToken();
		
		RPROReportService myRPROservice = new RPROReportService(config);
		
		return "";
	}
		
	public String getProductData(String filterOptions,String days,String extractOption)
	{
		System.out.println("Inside prod data:"+appName);
		ApiProcessorService api = new ApiProcessorService(config);
		api.setSessionAuthToken();
		
		AttributeDataApiService attributeData = new AttributeDataApiService(config);
		attributeData.getAttributeData(url);
		//System.out.println("Attribute data size: "+AttributeDataApiService.attrDataAll.size());
		
		List<String> targets = new ArrayList<String>();
		
			targets.add("WORLD_WIDE_GOLF");
			targets.add("BUDGET_GOLF");
			targets.add("CHANNEL_ADVISOR");
			finalReport.put("WORLD_WIDE_GOLF", new HashMap());
			finalReport.put("BUDGET_GOLF", new HashMap());
			finalReport.put("CHANNEL_ADVISOR", new HashMap());
			updateFilenames(targets);
		try 
		{
			for(String target : targets)
			{
				String searchParam = "";
				int count = 0;
				List myHeaders = invokeHeaderDataService(searchParam,target,days,extractOption,count);
				String prevUrl = invokeProductModelDataService(myHeaders,searchParam,target,days,extractOption,count);
				while(prevUrl!="")
				{		
					count++;
					prevUrl = invokeProductModelDataService(myHeaders, prevUrl,target,days,extractOption,count);
				}
				prevUrl = invokeProductDataService(myHeaders,searchParam,target,days,extractOption,count);
				while(prevUrl!="")
				{		
					count++;
					prevUrl = invokeProductDataService(myHeaders, prevUrl,target,days,extractOption,count);
				}
				
			}
			invokeReportDataService();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	public void invokeReportDataService() throws IOException{
		
		ReportWriteService myReportWriter = new ReportWriteService(config);
		myReportWriter.writeReport(finalReport, "Export_Report_" + fileDateFormat.format(new Date()) + ".txt");
	}
	
	public String invokeProductModelDataService(List myHeaders, String searchParam,String target, String days,String extractOption,
			int count) throws IOException
	{
		ProductDataWriteService productWriter = new ProductDataWriteService(config);
		ProductModelDataApiService productModelData = new ProductModelDataApiService(config);
		productModelData.getProductModelData(searchParam,target,days);
		
		String fileName = "ProductData_"+target+"_" + fileDateFormat.format(new Date()) + ".csv";
		System.out.println("Product Model");		
		productWriter.writeData(productModelData.productModelDataAll, myHeaders, AttributeDataApiService.attrDataAll, target, extractOption,fileName);
		HashMap myReport = (HashMap) finalReport.get(target);
		if (myReport.containsKey("Product_Model_Count")) {
			int myCount = Integer.parseInt((myReport.get("Product_Model_Count")) + "");
			myReport.put("Product_Model_Count", (myCount + productModelData.productModelDataAll.size()) + "");
		} else {
			myReport.put("Product_Model_Count", productModelData.productModelDataAll.size() + "" );
		}
		finalReport.put(target, myReport);
		return productModelData.previousUrl;
	}
	
	public String invokeProductDataService(List myHeaders, String searchParam,String target, String days,String extractOption,
			int count) throws IOException
	{
		ProductDataWriteService productWriter = new ProductDataWriteService(config);
		ProductDataApiService productData = new ProductDataApiService(config);
		productData.getProductData(searchParam,target,days);

		System.out.println("Product");
		String fileName = "ProductData_"+target+"_" + fileDateFormat.format(new Date()) + ".csv";
		productWriter.writeData(productData.productDataAll, myHeaders, AttributeDataApiService.attrDataAll, target, extractOption,fileName);
		HashMap myReport = (HashMap) finalReport.get(target);
		if (myReport.containsKey("Product_Count")) {
			int myCount = Integer.parseInt((myReport.get("Product_Count") + ""));
			myReport.put("Product_Count", (myCount + productData.productDataAll.size()) + "");
		} else {
			myReport.put("Product_Count", productData.productDataAll.size() + "" );
		}
		finalReport.put(target, myReport);
		return productData.previousUrl;
	}
	
	public List invokeHeaderDataService(String searchParam,String target, String days,String extractOption,
			int count) throws IOException
	{
		ProductDataWriteService productWriter = new ProductDataWriteService(config);
		String fileName = "ProductData_"+target+"_" + fileDateFormat.format(new Date()) + ".csv";
		List returnList = productWriter.writeHeader(AttributeDataApiService.attrDataAll, target, extractOption,fileName);
		HashMap myReport = (HashMap) finalReport.get(target);
		myReport.put("Attribute_Count", returnList.size() + "" );
		finalReport.put(target, myReport);
		return returnList;
	}
	
	public void updateFilenames(List<String> targets) {
		String folder = config.getOutboundfolder();
		for(String target : targets){
			String fileName = "ProductData_"+target+"_" + fileDateFormat.format(new Date()) + ".csv";
			
			File myFile = new File(folder + fileName);
			if(myFile.exists()) {
				String newFileName = "ProductData_"+target+"_" + fileDateFormat.format(new Date()) + "_prev.csv";
				File oldFile = new File(folder + newFileName);
				if(oldFile.exists()) {
					oldFile.delete();
				}
				myFile.renameTo(new File(folder + newFileName));
			}
		}
		
		String fileName = "Export_Report_" + fileDateFormat.format(new Date()) + ".txt";
		File myFile = new File(folder + fileName);
		if(myFile.exists()) {
			String newFileName = "Export_Report_" + fileDateFormat.format(new Date()) + "_prev.txt";
			File oldFile = new File(folder + newFileName);
			if(oldFile.exists()) {
				oldFile.delete();
			}
			myFile.renameTo(new File(folder + newFileName));
		}
	}
}
