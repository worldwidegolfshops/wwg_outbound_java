package com.wwgs.DataProcessor.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import com.wwgs.DataProcessor.configuration.GlobalConfiguration;
import com.wwgs.DataProcessor.data.Product;


public class ProductDataApiService {
	
	public ProductDataApiService(GlobalConfiguration config) {
		this.config = config;
	}
	
	private GlobalConfiguration config;
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	List<Product> productDataAll = new ArrayList<Product>();
	String previousUrl = "";
	
	

	public void getProductData(String url,String target,String days) 
	{
		System.out.println("Product data size.."+productDataAll.size());
		ApiProcessorService api = new ApiProcessorService(config);
		if(productDataAll.size() >= 1000)
		{
			api.setSessionAuthToken();
			System.out.println("NEW TOKEN");
			previousUrl = url;
			System.out.println("Greater than 10...");
			return;
		} 
		System.out.println("Getting product Data...Start"+dateFormat.format(System.currentTimeMillis()));
		String productDataOutput = null;
		String productUrl = null;
		productUrl=url;
		String encodedSearchParamWWG = "";
		String encodedSearchParamBG = "";
		String encodedSearchParamCA = "";
		try 
		{
			if(days==null)
			{
			encodedSearchParamWWG = URLEncoder.encode(config.getSearchParamWWGFull(), "UTF-8");
			encodedSearchParamBG = URLEncoder.encode(config.getSearchParamBudgetFull(), "UTF-8");
			encodedSearchParamCA = URLEncoder.encode(config.getSearchParamCAFull(), "UTF-8");
			}
			else
			{
				encodedSearchParamWWG = URLEncoder.encode(config.getSearchParamWWGDelta(), "UTF-8");
				encodedSearchParamWWG = encodedSearchParamWWG + days + URLEncoder.encode("}]}","UTF-8");
				encodedSearchParamBG = URLEncoder.encode(config.getSearchParamBudgetDelta(), "UTF-8");
				encodedSearchParamBG = encodedSearchParamBG + days + URLEncoder.encode("}]}","UTF-8");
				encodedSearchParamCA = URLEncoder.encode(config.getSearchParamCADelta(), "UTF-8");
				encodedSearchParamCA = encodedSearchParamCA + days + URLEncoder.encode("}]}","UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("product url from prop : "+config.getProducturlall()+" -- "+encodedSearchParamWWG);
		
		String productUrlWWG = config.getProducturlall() + encodedSearchParamWWG;
		String productUrlBG = config.getProducturlall() + encodedSearchParamBG;
		String productUrlCA = config.getProducturlall() + encodedSearchParamCA;
		
		//System.out.println("URL:"+productUrlWWG);
		
		if(productUrl.equals(null) || productUrl=="")
		{
				if(target.equals("WORLD_WIDE_GOLF")) {
					System.out.println("productURLWWG: " + productUrlWWG);
					productDataOutput = api.requestData(productUrlWWG);
				} else if(target.equals("BUDGET_GOLF")) {
					System.out.println("productURLBG: " + productUrlBG);
					productDataOutput = api.requestData(productUrlBG);
				} else if(target.equals("CHANNEL_ADVISOR")) {
					System.out.println("productURLCA: " + productUrlCA);					
					productDataOutput = api.requestData(productUrlCA);
				}
		}
		else
		{
			System.out.println("productURL: " + productUrl);
			productDataOutput = api.requestData(productUrl);
			
		}
		JSONObject attrDataResponseObj = new JSONObject(productDataOutput);
		processProductData(productDataOutput);
		if(attrDataResponseObj.getJSONObject("_links").has("next"))
		{
			productUrl = attrDataResponseObj.getJSONObject("_links")
					.getJSONObject("next").getString("href");
			getProductData(productUrl,"",null);
		}
		System.out.println("Getting product Data...End"+dateFormat.format(System.currentTimeMillis()));
	}

	public void processProductData(String productDataOutput) {
		System.out.println("Processing product Data...Start"+dateFormat.format(System.currentTimeMillis()));
		//System.out.println("OUTPUT DATA " + productDataOutput);
		HashMap allCategoryCache = new HashMap();
		JSONObject productDataResponseObj = new JSONObject(productDataOutput);
		// Get product data
		JSONArray productDataArray = productDataResponseObj.getJSONObject("_embedded").getJSONArray("items");
		for (int i = 0; i < productDataArray.length(); i++) {
			Product productData = new Product();
			String productId = (String) productDataArray.getJSONObject(i).get("identifier");
			boolean productEnabled = productDataArray.getJSONObject(i).getBoolean("enabled");
			productData.setProductId(productId);
		//	System.out.println(productDataArray.getJSONObject(i).get("parent"));
			if(productDataArray.getJSONObject(i).get("parent").toString() != "null")
				productData.setParentModelId((String) productDataArray.getJSONObject(i).get("parent"));
			if(productDataArray.getJSONObject(i).get("family").toString() != "null")
				productData.setProductFamily((String) productDataArray.getJSONObject(i).get("family"));
			productData.setEnabled(productEnabled);
			productData.setProductType("SKU");
			//add code for categories code retrieval, then get categories names
			
			if(!productDataArray.getJSONObject(i).getJSONArray("categories").isEmpty()) {
				String allCategoriesID = "";
				String allCategories = "";
				JSONArray allCategoryIDs = productDataArray.getJSONObject(i).getJSONArray("categories");
				for(int t=0; t < allCategoryIDs.length(); t++) {
					//System.out.println("CAT ID: " + allCategoryIDs.getString(t));
					try {
						allCategoriesID += allCategoryIDs.getString(t) + ", ";
						if(allCategoryCache.containsKey(allCategoryIDs.getString(t))) {
							allCategories += allCategoryCache.get(allCategoryIDs.getString(t)) + ",";
							//System.out.println("GOT CATEGORY FROM HASH: " + allCategoryIDs.getString(t));
						} else {
							ApiProcessorService api2 = new ApiProcessorService(config);
							//System.out.println("Getting Category " + allCategoryIDs.getString(t) + " "+dateFormat.format(System.currentTimeMillis()));
							String categoryUrl = config.getCategorieslabelurl();
							//System.out.println("CATEGORY URL: " + categoryUrl);
							
							String categoryApiOutput = api2.requestData(categoryUrl + URLEncoder.encode(allCategoryIDs.getString(t), "UTF-8"));
							//System.out.println("CATEGORY API OUTPUT: " + categoryApiOutput);
							JSONObject categoryResponseObj = new JSONObject(categoryApiOutput);
							JSONObject categoryLabelsObj = categoryResponseObj.getJSONObject("labels");
							if (categoryLabelsObj.length() != 0) {
								allCategories += categoryLabelsObj.get("en_US").toString() + ", ";
								allCategoryCache.put(allCategoryIDs.getString(t),categoryLabelsObj.get("en_US").toString());
							}
						}
					} catch (Exception e) {
						System.out.println("ERROR IN CAT RET: " + e);
					}
				}
				//System.out.println("CATEGORY IDS: " + allCategoriesID);
				//System.out.println("CATEGORY LABELS: " + allCategories);
				if(allCategoriesID.lastIndexOf(",") > 0)
					productData.setCategoriesID(allCategoriesID.substring(0, allCategoriesID.lastIndexOf(",")));
				else 
					productData.setCategoriesID(allCategoriesID);
				if(allCategories.lastIndexOf(",") > 0)
					productData.setCategoriesLabel(allCategories.substring(0, allCategories.lastIndexOf(",")));
				else 
					productData.setCategoriesLabel(allCategories);
			}
			
			
			JSONObject productAttributesList = productDataArray.getJSONObject(i).getJSONObject("values");
			HashMap<String, HashMap<String,Object>> productAttributes = new HashMap<String, HashMap<String,Object>>();
			
			for (String productAttrName : productAttributesList.keySet()) 
			{
				JSONArray attributeValue = (JSONArray) productAttributesList.get(productAttrName);
				boolean channelSpecificVal = false;
				if((attributeValue.length())>1)
					channelSpecificVal=true;
				HashMap<String, Object> productAttributesChannel = new HashMap<String, Object>();
				for (int j = 0; j < attributeValue.length(); j++) 
				{
					String channel = "common";
					String data = null;
					if(channelSpecificVal)
					{
						channel=String.valueOf(attributeValue.getJSONObject(j).get("scope"));
						if (attributeValue.getJSONObject(j).get("data") instanceof Boolean) {
							Boolean booleanValue = (Boolean) attributeValue.getJSONObject(j).get("data");
							productAttributesChannel.put(channel, booleanValue);
						} 
						
						
						else if(attributeValue.getJSONObject(j).get("data") instanceof JSONArray)
						{
						//	System.out.println("This might be a metric value??");
						}
						else
						{
						data = String.valueOf(attributeValue.getJSONObject(j).get("data"));
						productAttributesChannel.put(channel, data);
						}
					}
					else
					{
						if (attributeValue.getJSONObject(j).get("data") instanceof Boolean) {
							Boolean booleanValue = (Boolean) attributeValue.getJSONObject(j).get("data");
							productAttributesChannel.put(channel, booleanValue);
						} 
						else if(attributeValue.getJSONObject(j).get("data") instanceof Object)
						{
							Object dataTest = (Object) attributeValue.getJSONObject(j).get("data").getClass();
							//System.out.println(dataTest);
							if(attributeValue.getJSONObject(j).get("data") instanceof JSONObject)
							{
								JSONObject dataMetric = (JSONObject) attributeValue.getJSONObject(j).get("data");
								String valueMetric = (String) dataMetric.get("amount");
								String unitMetric = (String) dataMetric.get("unit");
								int dataLength = dataMetric.length();
							//	System.out.println("data length.."+dataLength+" :value: "+valueMetric+" :unitMetric: "+unitMetric);
							//	System.out.println("This might be a metric value??"+dataTest.toString());
								List<String> dataWithMetric = new ArrayList<String>();
								dataWithMetric.add(valueMetric);
								dataWithMetric.add(unitMetric);
								productAttributesChannel.put(channel, dataWithMetric);
							}
							else
							{
								data = String.valueOf(attributeValue.getJSONObject(j).get("data"));
								productAttributesChannel.put(channel, data);
							}
						}
						else
						{
						data = String.valueOf(attributeValue.getJSONObject(j).get("data"));
						productAttributesChannel.put(channel, data);
						}
					}
				}
				productAttributes.put(productAttrName, productAttributesChannel);
			}
			productData.setAttributeValue(productAttributes);
			productDataAll.add(productData);
			
		}
		System.out.println("Processing product Data...End"+dateFormat.format(System.currentTimeMillis()));
	
	}
}

