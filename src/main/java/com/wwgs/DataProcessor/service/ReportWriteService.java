package com.wwgs.DataProcessor.service;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.opencsv.CSVWriter;
import com.wwgs.DataProcessor.configuration.GlobalConfiguration;
import com.wwgs.DataProcessor.data.AttributeListMetaData;
import com.wwgs.DataProcessor.data.AttributeMetaData;
import com.wwgs.DataProcessor.data.Product;

public class ReportWriteService {
	private final GlobalConfiguration config;
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	
	public ReportWriteService(GlobalConfiguration config) {
		this.config = config;
	}
	
	public void writeReport(HashMap reportData, String fileName) throws IOException {
		System.out.println("Writing Report...Start: " + dateFormat.format(System.currentTimeMillis()));
		//System.out.println("Size product data.."+productDataAll.size());
		String folder = config.getOutboundfolder();
		System.out.println("folder to write.."+folder);
		System.out.println(fileName);
		FileWriter outputfile = new FileWriter(folder+fileName, true);
		Iterator it = reportData.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			outputfile.write("Target: " + pair.getKey() + "\n");
			HashMap myValue = (HashMap) pair.getValue();
			Iterator it2 = myValue.entrySet().iterator();
			while(it2.hasNext()) {
				Map.Entry pair2 = (Map.Entry)it2.next();
				outputfile.write((pair2.getKey() + "").replace("_", " ") + ": " + pair2.getValue() + "\n");
			}
			outputfile.write("\n\n");
		}
		outputfile.close();
		System.out.println("Completed Report...End: " + dateFormat.format(System.currentTimeMillis()));
	}
}
