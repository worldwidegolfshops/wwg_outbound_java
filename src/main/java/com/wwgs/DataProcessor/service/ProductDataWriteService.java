package com.wwgs.DataProcessor.service;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.opencsv.CSVWriter;
import com.wwgs.DataProcessor.configuration.GlobalConfiguration;
import com.wwgs.DataProcessor.data.AttributeListMetaData;
import com.wwgs.DataProcessor.data.AttributeMetaData;
import com.wwgs.DataProcessor.data.Product;


public class ProductDataWriteService 
{
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	
	private final GlobalConfiguration config;
	
	public ProductDataWriteService(GlobalConfiguration config) {
		this.config = config;
	}
			
	public void writeData(List<Product> productModelDataAll, List<String> listProductHeader, List<AttributeMetaData> attributeDataAll,
			String target, String extractOption,String fileName) 
					throws IOException {
		
		System.out.println("Writing Data...Start: " + dateFormat.format(System.currentTimeMillis()));
		//System.out.println("Size product data.."+productDataAll.size());
		String folder = config.getOutboundfolder();
		System.out.println("folder to write.."+folder);
		System.out.println(fileName);
		FileWriter outputfile = new FileWriter(folder+fileName, true);
		CSVWriter writer = new CSVWriter(outputfile, '|','"','"',"\n");

		for (int j = 0; j < productModelDataAll.size(); j++) 
		{
			if(productModelDataAll.get(j).getProductFamily() != null)
			{
				ArrayList<String> productDataCSV = new ArrayList<String>();
				productDataCSV.add(productModelDataAll.get(j).getProductType());
				productDataCSV.add(productModelDataAll.get(j).getParentModelId());
				productDataCSV.add(productModelDataAll.get(j).getProductId());
				productDataCSV.add(productModelDataAll.get(j).getCategoriesID());
				productDataCSV.add(productModelDataAll.get(j).getCategoriesLabel());
				for (int k = 1; k < listProductHeader.size(); k++) {
					String attrVal = "";
					//System.out.println("List val:"+list.get(k));
					//System.out.println("productModelDataAll.size(): " + productModelDataAll.size());
					//System.out.println("listProductHeader.size(): " + listProductHeader.size());
					for (int i = 0; i < attributeDataAll.size(); i++) {
						if (attributeDataAll.get(i).getAttrOptionsMaps().get(listProductHeader.get(k)) != null
								&& attributeDataAll.get(i).getType().equals("pim_catalog_simpleselect")) 
						{
							List<AttributeListMetaData> attrMetaData = attributeDataAll.get(i).getAttrOptionsMaps().get(listProductHeader.get(k));

							for (int opt = 0; opt < attrMetaData.size(); opt++) 
							{
								if(productModelDataAll.get(j).getAttributeValue().get(listProductHeader.get(k)) != null)
								{
									String prodAttrVal = getChannelAttr(target,productModelDataAll.get(j).getAttributeValue().get(listProductHeader.get(k)));
									if (attrMetaData.get(opt).getCode().equals(prodAttrVal)) 
									{
										attrVal = attrMetaData.get(opt).getLabel();
									}
								}
							}
						}
					}
					if (attrVal == "" && productModelDataAll.get(j).getAttributeValue().get(listProductHeader.get(k)) != null)
					{
						String attrValChannel = getChannelAttr(target,productModelDataAll.get(j).getAttributeValue().get(listProductHeader.get(k)));								
						if(attrValChannel!=null) {
							if(attrValChannel.contains("[")) {
								attrValChannel = attrValChannel.substring(2, attrValChannel.length()-2);
							}
							if (attrValChannel.contains("amount") && attrValChannel.contains("currency")) {
								//System.out.println("AN AMOUNT: " + attrValChannel + "\nUPDATE: " + attrValChannel.substring(attrValChannel.indexOf(":") + 1, attrValChannel.lastIndexOf(",") - 1));
								attrValChannel = "$" + attrValChannel.substring(attrValChannel.indexOf(":") + 2, attrValChannel.lastIndexOf(",") - 1);
							}
							productDataCSV.add(attrValChannel);
						}else
							productDataCSV.add("");				
					}
					else
					{
						if(attrVal.contains("[")) {
							attrVal = attrVal.substring(2, attrVal.length()-2);
						} 
						if (attrVal.contains("amount") && attrVal.contains("currency")) {
							attrVal = attrVal.substring(14, attrVal.indexOf(",") - 2);
						}
						productDataCSV.add(attrVal);
					}
				}
				writer.writeNext((String[]) productDataCSV.toArray(new String[listProductHeader.size()]));
			}		

		}
		writer.close();
		System.out.println("Completed Data...End: " +dateFormat.format(System.currentTimeMillis()));
	}
	
	public String getChannelAttr(String target,HashMap<String, Object> attrData)
	{
		String attrValue = null;
		if(attrData.get(target) != null)
		{
			if(attrData.get(target) instanceof Boolean)
			{
				Boolean attrValueboolean = (Boolean) attrData.get(target);
				attrValue = attrValueboolean.toString();
			}else if(attrData.get(target) instanceof ArrayList)
			{
				List<String> attrValuesMetric = (List<String>) attrData.get(target);
				attrValue = (String) attrValuesMetric.get(0);
			}
			else {
				attrValue = (String) attrData.get(target);
			}
		}
		else
		{
			if(attrData.get("common") instanceof Boolean)
			{
				Boolean attrValueboolean = (Boolean) attrData.get("common");
				attrValue = attrValueboolean.toString();
			}
			else if(attrData.get("common") instanceof ArrayList)
			{
				List<String> attrValuesMetric = (List<String>) attrData.get("common");
				attrValue = (String) attrValuesMetric.get(0);
			}
			else {
				attrValue = (String) attrData.get("common");
			}
		}
		return attrValue;
	}
	public Set<String> loadConfigs(String target,String extractOption)
	{
			Properties prop = new Properties();
			Set<String> productHeaders = new HashSet<String>();
			String CAProperties = config.getResourcefolder()+"CAAttributeList.properties";
			String BGProperties = config.getResourcefolder()+"XCartAttributeList.properties";
			String CommonProperties = config.getResourcefolder()+"AttributeList.properties";
			try {
				if(target.equals("CHANNEL_ADVISOR"))
					prop.load(new FileInputStream(CAProperties));
				else if(target.equals("BUDGET_GOLF"))
					prop.load(new FileInputStream(BGProperties));
				else
					prop.load(new FileInputStream(CommonProperties));
			    Enumeration<Object> keys = prop.keys();
			    while (keys.hasMoreElements()) 
			    {
			        String key = (String)keys.nextElement();
			        productHeaders.add(key);
			    }
			 } catch (IOException e) {
			    e.printStackTrace();
			}
			return productHeaders;
	
	}
	
	
	public List writeHeader(List<AttributeMetaData> attributeDataAll, String target, String extractOption,String fileName) 
					throws IOException {
	
		System.out.println("Writing Header...Start: " +dateFormat.format(System.currentTimeMillis()));
		//System.out.println("Size product data.."+productDataAll.size());
		String folder = config.getOutboundfolder();
		System.out.println("folder to write.."+folder);
		System.out.println(fileName);
		FileWriter outputfile = new FileWriter(folder+fileName, true);
		CSVWriter writer = new CSVWriter(outputfile, '|','"','"',"\n");
		Set<String> productHeaders = new HashSet<String>();
		productHeaders = loadConfigs(target,extractOption);
		List<String> listProductHeader = new ArrayList<String>(productHeaders);
		Collections.sort(listProductHeader);
		List<String> header = new ArrayList<String>();
		header.add(0, "Product Type");
		header.add(1, "Parent SKU");
		header.add(2, "SKU");
		header.add(3, "Category IDs");
		header.add(4, "Categories");
		String attrName = null;
		for (int k = 1; k < listProductHeader.size(); k++) 
		{
			for (int i = 0; i < attributeDataAll.size(); i++)
			{
				attrName = attributeDataAll.get(i).getAttrCdName().get(listProductHeader.get(k));
				if(attrName!=null) break;
			}
			//System.out.println("Attribute Name:"+attrName);
			if (attrName != null)
				header.add(attrName);
			else
				header.add(listProductHeader.get(k));

		}
		writer.writeNext(header.toArray(new String[listProductHeader.size()]));
		writer.close();
		System.out.println("Completed Header...End: "+dateFormat.format(System.currentTimeMillis()));
		return listProductHeader;
	}

}

