package com.wwgs.DataProcessor.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wwgs.DataProcessor.service.ProductDataAccessService;

@RequestMapping("dev/product/data")
@RestController
@Profile("dev")
public class ProductControllerDev {

	private final ProductDataAccessService productService;
	String filterOptions = null;
	String extractOption = null;
	
	@Autowired
	public ProductControllerDev(ProductDataAccessService productService) {
		this.productService = productService;
	}
	
	//@Scheduled(cron = "15 23 * * * ?")
	//@Scheduled(initialDelay = 1000, fixedDelayString = "${config.schedule}")
	public void accessProductDataAllCmd()
	{
		String filterOptions = "All";
		
		String days = "1";
		System.out.println("Inside scheduled job run..");
		productService.getProductData(filterOptions,days,extractOption);
	}
	
	//@Scheduled(cron = "15 23 * * * ?")
	//@Scheduled(initialDelay = 1000, fixedDelayString = "${config.schedule}")
	@GetMapping(path="/all")
	public String accessProductDataAll()
	{
		System.out.println("inside all...");
		String filterOptions = "All";
		String days = null;
		return productService.getProductData(filterOptions,days,extractOption);
	}
	
	@GetMapping(path="/delta/{days}")
	public String accessProductDataDelta(@PathVariable("days") String days)
	{
		return productService.getProductData(filterOptions,days,extractOption);
	}
	
	@GetMapping(path="/custom/all")
	public String accessProductCustomAll()
	{
		extractOption = "Custom";
		filterOptions = "custom";
		String days = null;
		return productService.getProductData(filterOptions,days,extractOption);
	}
	
	@GetMapping(path="/custom/delta/{days}")
	public String accessProductCustomDelta(@PathVariable("days") String days)
	{
		extractOption = "Custom";
		filterOptions = "custom";
		return productService.getProductData(filterOptions,days,extractOption);
	}
	
}
